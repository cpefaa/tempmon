# Fill in the variables below, and copy this file to 'acct_info.py' in
# the same directory as the tempstick.py file
tempstick_username = 'USERNAME'
tempstick_password = 'PASSWORD'

# If false, the script will clean up after itself; if true, it will
# retain (and print the location of) the temporary directory
retain_temp_files = False

