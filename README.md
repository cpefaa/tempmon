# README: EFAA Temperature Monitoring

This project documents the system Cardinal Peak put in place to
provide temperature monitoring and reporting to EFAA’s refrigerators
and freezers.

Additional documentation can be found in the file 
"EFAA cold storage monitoring reqts 2017--11-16.doc" located in this directory.

## Device

In February 2018, we purchased 10 “Tempsticks” from [Ideal
Sciences](http://idealsciences.com/) , at approximately $150 each.

The Tempstick is relatively small and is placed directly into the
refrigerator or freezer being monitored.  It is battery powered (by
two lithium AA batteries, lithium being required because of the cold),
and batteries last about a year.  The unit wakes up once per hour,
connects to EFAA’s Wi-Fi network, and transmits the current
temperature and humidity up to the Tempstick cloud server.

The Tempsticks are installed in the following locations at EFAA:

	1. Upstairs Meat Freezer
	2. Upstairs Walk-in Cooler
	3. Upstairs Dry Air
	4. Downstairs Refrigerator #1
	5. Downstairs Freezer #2
	6. Downstairs Freezer #3
	7. Downstairs Freezer #4
	8. Downstairs Freezer #5
	9. Downstairs Dry Air
	10. “Howdys Office” — a test unit that was kept at Cardinal Peak and is connected to a different back-end account

## TemperatureStick.com cloud

To configure the tempsticks, see historical or current temperatures,
and set or edit alerts, log into the TemperatureStick.com website.
Usernames and passwords are as follows:

	* Main account (units 1-9): username tempmonitor@efaa.org
	* Test account (unit 10): username howdy_pierce@yahoo.com
	
As of this writing, the passwords to these two accounts are stored as
configuration variables in the file /home/howdy/tempmon/acct_info.py 
on the cpefaa server, and they are also stored in Howdy’s LastPass.

## Alerts

The TemperatureStick.com cloud can be configured to send email and
text alerts in the case temperatures are out of range, or in the case
where a device was expected to check in, and didn’t.  Unfortunately in
our testing we have found that these alerts aren’t 100% reliable.

To see the current configuration for alerts, log into
TemperatureStick.com and select Alerts from the left-hand menu

## Cardinal Peak server

The TemperatureStick.com server doesn’t have the ability to generate a
periodic report, so Cardinal Peak created a python script that
automatically generates a PDF report.  One report is generated once
per week (early on Monday morning, covering the previous Monday
through Sunday), and another report is generated once per month (early
on the morning of the first of the month).

Stdout and stderr from the report script is redirected to the file
`/home/howdy/tempstick.log`, so if things aren’t working, look here
first.  The most likely cause of breakage would be that something
changed in the HTML structure of the webpages at TemperatureStick.com;
our python script needs to parse these, and while we tried to make
things generic, it’s unfortunately coupled to the specifics of how the
TempStick people wrote their webpages.

The server for this reporting infrastructure is a VM located at
cpefaa.cardinalpeak.com, managed by Nick. Login credentials are
stored in Howdy’s LastPass account.

Most configuration variables are stored directly in the Python script,
located at `/home/howdy/tempmon/tempstick.py`.  The destination email
for each report is found in the `crontab` file in the same
subdirectory.

### cpefaa.cardinalpeak.com installation details

We started with an Ubuntu VM running Ubuntu 16.04.3 LTS. On top of
this VM, we installed the following packages:
`sudo apt-get install python3-pip xvfb libfontconfig1 libxrender1`

Then I installed the skhtmltopdf binary, version 0.12.4, 64-bit Linux,
from the project page:
[wkhtmltopdf](https://wkhtmltopdf.org/downloads.html) . This same
binary is checked into the git repository for this project. After
untarring this binary, we moved all the files into the appropriate
places under /usr/local and chown’ing as needed.

Then, using pip3 we installed the following Python packages:
`pip3 install jinja2 lxml matplotlib`

The project uses Jinja2 version 2.10, lxml version 4.1.1, and
matplotlib version 2.2.2

## License

Cardinal Peak's work for this project (meaning primarily the files 
in this directory) is freely available under the GNU General Public 
License, version 3 or later.  See the file COPYING for more.
