#!/usr/bin/env python3
#
# Copyright 2018, Cardinal Peak, LLC
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/.

import acct_info       # for username & password to the tempstick server
import requests
from lxml import html
from datetime import date, datetime, timedelta, time
import sys
import os
import csv
from jinja2 import Environment, FileSystemLoader
import tempfile
import shutil
import subprocess
import smtplib
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formatdate
import matplotlib
# Necessary to have this early, to prevent matplotlib's interactive mode
matplotlib.use('Agg')
import matplotlib.pyplot as plt      # nopep8 (due non-import line above)
import matplotlib.dates as mdates    # nopep8 (due non-import line above)


#############################################################################
#
# Config variables
#
#############################################################################

# login credentials for the temperaturestick.com server
tempstick_host = 'temperaturestick.com'
tempstick_dashboard_url = 'https://{}/sensors/dashboard'.format(tempstick_host)
tempstick_ajax_path = '/sensors/templates/graphs/ajax.php'
# timeout in sec; needs to be long because server is slow
tempstick_timeout = 120.0

# These are the target times for the 'morning' and 'afternoon'
# readings, respectively.  What is included in the report will be the
# first reading of the day that is after the target time
morning_tgt_time = time(9)
afternoon_tgt_time = time(15)

# Location of the log_template.html and efaa_logo.png files
script_dir = os.path.dirname(os.path.abspath(__file__))
log_template_file = 'log-template.html'
logo_file = 'efaa_logo.png'

# Name of the emailed file
output_pdf = 'Temperature_Report_{:%Y_%m_%d}.pdf'

# Output email formatting
email_subj = "Temperature Report ({:%-m/%-d/%Y} to {:%-m/%-d/%Y})"
email_body = """
The EFAA Temperature Report for the period {:%-m/%-d/%Y} to
{:%-m/%-d/%Y} is attached.  For additional detail, go <a
href="{}">here</a> and log in as user {}.
<br>
<br>
For questions, email info@cardinalpeak.com.
<br>
<br>
"""
email_server = "mercury.cardinalpeak.com"
email_from = 'info@cardinalpeak.com'

# mapping between devicename and the green horizontal temperature line. If the
# devicename isn't on this list, then the green line will not be drawn
temp_alert_line = {"Downstairs Refrigerator 1": 45.0,
                   "Downstairs Freezer 2": 23.0,
                   "Downstairs Freezer 3": 23.0,
                   "Downstairs Freezer 4": 15.0,
                   "Downstairs Freezer 5": 15.0,
                   "Upstairs Meat Freezer": 38.0,
                   "Walk-in Cooler": 48.0,
                   "Upstairs Dry Air": 85.0,
                   "Howdys Office": 65.0}

# a list of the grey horizontal temperature lines that should be drawn
# on all graphs
static_temp_lines = [0.0, 20.0, 40.0, 60.0, 80.0]


#############################################################################


def parse_for_battery(tree, sensors):
    """Parse the returned page for battery percentage and last
    check-in date for each sensor, and add these to the sensors
    list.

    Parameters
    ----------
    tree
       The full lxml tree
    sensors
       the list of sensors, each one a dict

    Returns
    -------
    If possible, fills in the 'battery_pct' and 'battery_date'
    attributes of the sensors list
    """
    # The code below is ugly and brittle. The HTML structure returned
    # by the server is not tagged with id's, so we go off of class
    # names and relative structure (yuck).
    #
    # If we cannot parse a battery_pct or date, the code is designed
    # to fail silently; the report will end up with 'n/a' in that
    # case.
    #
    # The HTML structure is a list that looks something like this:
    #
    #     div(cl: row): 2 children   <-- this is 'par_node' in code below
    #      div(cl: small-6 columns): 3 children
    #       div: 1 children
    #        span(ID:)[Downstairs Freezer 4]  <-- 'name_node' in code below
    #              note: text = "ID:" and tail = the actual name
    #       label(Check-In)
    #       span(cl: check-in-date)(Mar 01, 01:40 pm)  <-- 'dt_node' in code
    #      div(cl: small-6 columns): 3 children
    #       div(cl: row collapse-mobile): 2 children
    #        div(cl: small-6 columns)(Temp:)
    #        div(cl: small-6 columns)(-17° F)
    #       div(cl: row collapse-mobile): 2 children
    #        div(cl: small-6 columns)(Humidity:)
    #        div(cl: small-6 columns)(26.90%)
    #       div(cl: row collapse-mobile): 2 children
    #        div(cl: small-6 columns)(Battery:)
    #        div(cl: small-6 columns)(100%) <-- 'batt_node' in code below
    #
    act_sens = tree.xpath('//div[contains(@class, "active-sensors")]')
    if (not act_sens):
        print("Didn't find active-sensors div, can't read battery pcts")
        return

    # At this point we've got ahold of a parent div, which has a list
    # of the structure above. Iterate through that looking for our
    # elements

    name_xpath = './/span[contains(text(),"ID")]'
    check_in_xpath = './/span[@class="check-in-date"]'
    battery_pct_xpath = './/div[contains(text(), "Battery")]'
    date_format = '%b %d, %I:%M %p'

    for name_node in act_sens[0].xpath(name_xpath):
        name_txt = name_node.tail.strip()
        sensor = next((s for s in sensors if s['name'] == name_txt), None)
        if (not sensor):
            continue

        # at this point, name_node matches sensor
        try:
            par_node = name_node.getparent().getparent().getparent()
            dt_node = par_node.xpath(check_in_xpath)[0]
            batt_node = par_node.xpath(battery_pct_xpath)[0].getnext()

            # if we get to here, we got all the nodes successfully
            batt_txt = batt_node.text.strip().replace('%', '')
            sensor['battery_pct'] = int(batt_txt)

            dt_txt = dt_node.text.strip()
            d = datetime.strptime(dt_txt, date_format)
            sensor['battery_date'] = d.replace(year=datetime.now().year)
        except Exception as e:
            print("Error parsing battery pct for {}".format(name_txt))
            print(e, flush=True)
            continue


# To get the CSV, we need to do the following three things, in order
#
#  1) HTTP GET on the dashboard URL
#
#  2) From this - HTTP POST login info to a URL defined in the HTML
#      returned in #1. The POST will return a web page that contains
#      the battery level & other data
#
#  3) GET the CSV from the download-csv URL, encoding the sensor name
#      & dates into this URL
#
def get_csvs(output_loc: str,
             start: date,
             end: date) -> list:
    """Download CSV files from the TemperatureStick.com server.

    One CSV file per sensor will be downloaded.

    Parameters
    ----------
    output_loc
       Path to a temporary directory. This function will write one CSV
       per configured sensor, with the file name following the pattern
       (id)-(startdate)-(enddate).csv
    start, end:
       Date objects that define the range of interest, inclusive

    Returns
    -------
    The 'sensordata': A list of dicts, with one entry in the list per
    sensor. Each sensordata entry has the following keys:

    csvfile : string
       Filename of the retrieved CSV file, located in output_loc
    name : string
       The friendly name of this sensor
    id : string
       The TemperatureStick.com internal ID for this sensor
    start : date
       The first date requested (same as 'start' input parameter)
    end : date
       The last date requested (same as 'end' input parameter)
    battery_pct : int
       The battery percentage, as an int in the range [0,100], or None
       if not known
    battery_date : datetime
       The date the battery percentage was last read, or None
    report_date : datetime
       The time the CSV data was fetched

    In addition to the keys above, the following additional keys are
    added to 'sensordata' by functions called later. They are
    described here for completeness:

    daily_readings
       The processed_data list-of-dicts, as returned from
       process_raw_data. Added by the import_and_process_csv call
    htmlfile
       Filename of the generated HTML file.  Added by
       create_html_report.
    """

    # return list
    sensors = []

    # establish a session
    session = requests.Session()

    # Step 1: Get the dashboard URL and determine the URL to POST the
    # login info to
    r = session.get(tempstick_dashboard_url, timeout=tempstick_timeout)
    r.raise_for_status()

    # Try to extract the next URL and the form field names
    # programmatically, but fall back to hardcoded URL & fields
    try:
        # Hopefully there is exactly one form on the page!
        tree = html.fromstring(r.content)
        forms = tree.xpath('//form')
        if len(forms) != 1:
            raise Exception()
        login_url = forms[0].action

        # recursively find all input elements in form
        login_info = {}
        for c in forms[0].findall('.//input'):
            if c.type == 'hidden':
                login_info[c.name] = c.value
            elif c.type == 'password':
                login_info[c.name] = acct_info.tempstick_password
            elif c.type == 'text' and c.name == 'email':
                login_info[c.name] = acct_info.tempstick_username

    except:
        # it didn't work, for whatever reason: hardcoded fallback values
        print("Couldn't extract form data from page; falling back to"
              "hardcoded form structure")
        login_url = 'https://{}/sensors/process/user/login.php'.\
                    format(tempstick_host)
        login_info = {'form_name': 'Login',
                      'form_slug': 'Login_Form',
                      'form_request_url': '/sensors/',
                      'email': acct_info.tempstick_username,
                      'password': acct_info.tempstick_password,
                      'email_': ''}

    # Step 2: Post login info
    r = session.post(login_url, data=login_info, timeout=tempstick_timeout)
    r.raise_for_status()

    # Get list of sensors. These are tuples: (Id, FriendlyName)
    tree = html.fromstring(r.content)
    sel_sens_id = tree.xpath('//*[@id="selected_sensor_id"]')
    if len(sel_sens_id) != 1:
        raise Exception()

    for c in sel_sens_id[0].findall('.//option'):
        sensors.append({'id': c.attrib['value'],
                        'name': c.text,
                        'start': start,
                        'end': end,
                        'battery_pct': None,
                        'battery_date': None})

    # Parse for the battery data, if possible
    parse_for_battery(tree, sensors)

    # Step 3: Now get the CSV for the specified period. The URL we
    # need is embedded in an HTML snippet dynamically injected into
    # the previously-fetched page via an ajax call. So for each
    # sensor, we need to make that ajax call, and then parse the URL
    # (which contains a token) out of that.
    #
    # The Ajax URL is hardcoded in the code below, but could be parsed
    # from what is returned above if this approach proves brittle.
    ret_sensors = []
    for s in sensors:
        print("Fetching sensor {} ({})".format(s['name'], s['id']), flush=True)

        # get ajax snippet
        params = {'sensor': s['id'],
                  'start' : start.strftime("%Y-%m-%d"),
                  'end'   : end.strftime("%Y-%m-%d") }
        url = 'https://{}{}'.format(tempstick_host, tempstick_ajax_path)
        r = session.get(url, params=params, timeout=tempstick_timeout)
        r.raise_for_status()
        tree = html.fromstring(r.content)

        # Right now, there are two <a> elements returned from this
        # ajax, and they're both the same - one is for the download
        # button for the temperature graph and one is for the download
        # button for the humidity graph. Both give the URL to fetch a
        # CSV file containing both temperature and humidity
        a_elems = tree.findall('.//a')
        if len(a_elems) == 0:
            expl = tree.findall('.//p')
            if len(expl) != 0 and expl[0].text:
                print("  Error: {}".format(expl[0].text))
            else:
                print("  No data found for this sensor, dropping it")
            continue
        url = a_elems[0].attrib['href']
        print("  URL: {}".format(url))

        r = session.get(url, stream=True, timeout=tempstick_timeout)
        r.raise_for_status()

        s['csvfile'] = '{}-{:%Y%m%d}-to-{:%Y%m%d}.csv'.\
                       format(s['id'], start, end)
        with open(os.path.join(output_loc, s['csvfile']), 'wb') as fd:
            for chunk in r.iter_content(chunk_size=1024):
                fd.write(chunk)
        s['report_date'] = datetime.now()
        ret_sensors.append(s)

    return ret_sensors


# Stole this function from thibault ketterer at
# https://stackoverflow.com/questions/1060279/iterating-through-a-range-of-dates-in-python
def daterange(start_date, end_date):
    for n in range(int((end_date - start_date).days)+1):
        yield start_date + timedelta(n)


def process_raw_data(rawdata: dict,
                     start: date,
                     end: date):
    """Process the data extracted from the CSV file into the data we're
    going to output

    Parameters
    ----------
    rawdata: dict
       key = date as date ("2018-01-30")
       value = list of tuples
         each tuple is (time as datetime, temp as float, humidity as float)

       For instance, for two dates and two readings per date:

       {'2018-01-30' : [('11:47:37', -2.5, 16.8),
                        ('12:43:47', -4.4, 17.7)],
        '2018-01-31' : [('09:58:58', 77.8, 13.73),
                        ('10:48:43', 10.2, 9.4)]}
    start, end:
       Date objects that define the range of interest, inclusive

    Returns
    -------
    The "processed_data": A list of dicts, one entry per day between
    start and end, following this:

    [ { 'date': (date object e.g., '11/1/17'),
        'morning_temp' : -0.3,
        'morning_time' : (datetime object '10:03am'),
        'afternoon_temp' : -1.2,
        'afternoon_time' : (datetime object '10:03am'),
        'average_temp' : -1.3,
        'average_time' : (datetime object '10:03am'),
        'minimum_temp' : -4.7,
        'minimum_time' : (datetime object '10:03am'),
        'maximum_temp' : 4.0,
        'maxumum_time' : (datetime object '10:03am'),
        'num_readings' : (int, e.g., 24) } ]

    If there are no readings for a given day in rawdata, there will be
    an entry for that day in the output, but all the fields except
    'date' and 'num_readings' will be None.

    """
    retlist = []

    for dt in daterange(start, end):
        dd = {'date': dt,
              'morning_temp': None,
              'morning_time': None,
              'afternoon_temp': None,
              'afternoon_time': None,
              'average_temp': None,
              'minimum_temp': None,
              'minimum_time': None,
              'maximum_temp': None,
              'maximum_time': None,
              'num_readings': 0}

        morning_tgt = datetime.combine(dt, morning_tgt_time)
        afternoon_tgt = datetime.combine(dt, afternoon_tgt_time)

        avg_sum = 0.0
        if dt not in rawdata:
            retlist.append(dd)
            continue

        for e in rawdata[dt]:
            (rtime, rtemp) = e[0:2]

            # morning
            if (rtime > morning_tgt and rtime < afternoon_tgt and
                    (not dd['morning_time'] or
                     rtime < dd['morning_time'])):
                dd['morning_temp'] = rtemp
                dd['morning_time'] = rtime

            # afternoon
            if (rtime > afternoon_tgt and
                    (not dd['afternoon_time'] or
                     rtime < dd['afternoon_time'])):
                dd['afternoon_temp'] = rtemp
                dd['afternoon_time'] = rtime

            # average
            avg_sum += rtemp

            # min
            if (not dd['minimum_temp'] or rtemp < dd['minimum_temp']):
                dd['minimum_temp'] = rtemp
                dd['minimum_time'] = rtime

            # max
            if (not dd['maximum_temp'] or rtemp > dd['maximum_temp']):
                dd['maximum_temp'] = rtemp
                dd['maximum_time'] = rtime

            # num_readings
            dd['num_readings'] += 1

        if (dd['num_readings'] > 0):
            dd['average_temp'] = avg_sum / dd['num_readings']

        retlist.append(dd)

    return retlist


def create_graph(rawdata: dict,
                 name: str,
                 start: date,
                 end: date,
                 output_file: str):
    """Process the rawdata to create a graph as a png file.

    Parameters
    ----------
    rawdata: dict
       See definition to process_raw_data, above
    name: 
       The devicename for this device, for looking up the green line
    start, end:
       Date objects that define the range of interest, inclusive
    output_file
       The full path to the png file to be created
    """

    dt = list()
    tp = list()
    for daydata in sorted(rawdata.values()):
        for v in daydata:
            dt.append(mdates.date2num(v[0]))
            tp.append(v[1])

    numdays = (end-start).days + 1

    fig, ax = plt.subplots(figsize=(8.0, 3.5), dpi=100)
    ax.plot(dt, tp, "b-", linewidth=0.5)     # b here means blue

    # x-axis: ticks, labels, range
    ival = 7 if numdays > 10 else 1
    ax.xaxis.set_major_locator(mdates.DayLocator(interval=ival))
    ax.xaxis.set_major_formatter(mdates.DateFormatter("%-m/%-d"))
    ax.xaxis.set_minor_locator(mdates.DayLocator())

    mst = mdates.date2num(start)
    mend = mdates.date2num(end + timedelta(days=1))
    ax.set_xlim(mst, mend)

    # y-axis: ticks, labels, range
    ax.yaxis.set_major_locator(matplotlib.ticker.MultipleLocator(10.0))
    ax.set_ylim(-20, 85)

    ax.tick_params(axis='both', which='major', labelsize=8)

    plt.setp(ax.spines.values(), linewidth=0.5)

    # highlight weekends
    for d in [start + timedelta(days=x) for x in range(numdays)]:
        if (date.weekday(d) in [5, 6]):
            md = mdates.date2num(d)
            ax.axvspan(md, md+1, facecolor='#E0E0E0', edgecolor='none')

    # add grey static dashed lines 
    for x in static_temp_lines:
        ax.axhline(x, ls="--", linewidth=0.5, color='#C0C0C0')

    # add green alert line, also dashed
    if name in temp_alert_line:
        ax.axhline(temp_alert_line[name], ls="-", linewidth=0.5,
                   color='#40AC3E')
    

    # write it out
    plt.tight_layout()
    plt.savefig(output_file)


def import_and_process_csv(sensor: dict,
                           output_loc: str):
    """Process the CSV data for a single sensor.

    Parameters
    ----------
    sensor
       A sensor_dict following the structure returned by get_csvs
    output_loc
       The temporary directory containing the CSVs and HTML files.

    Returns
    -------
    On return, the passed-in dict will have the following new key added:

    daily_readings
       The processed_data list-of-dicts, as returned from process_raw_data
    """

    # import the csv file into a dict following the rawdata definition to
    # process_raw_data
    rawdata = {}
    with open(os.path.join(output_loc, sensor['csvfile']), 'r') as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            # ignore header row
            if row[0].startswith("Check In"):
                continue
            dt = datetime.strptime(row[0], "%Y-%m-%d %H:%M:%S")
            temp = float(row[1])
            humidity = float(row[2])
            if dt.date() in rawdata:
                rawdata[dt.date()].append((dt, temp, humidity))
            else:
                rawdata[dt.date()] = [(dt, temp, humidity)]

    sensor['graphfile'] = os.path.splitext(sensor['csvfile'])[0] + ".png"
    create_graph(rawdata, sensor['name'], sensor['start'], sensor['end'],
                 os.path.join(output_loc, sensor['graphfile']))

    sensor['daily_readings'] = \
        process_raw_data(rawdata, sensor['start'], sensor['end'])


def create_html_report(sensor_data: dict,
                       output_loc: str):
    """Create the HTML report from the processed data

    Parameters
    ----------
    sensor_data
       See the sensor_data dict definition in get_csvs
    output_loc
       The temporary directory containing the CSVs and HTML files.

    Returns
    -------
    Upon return, the sensor_data dict will updated with a new key:

    htmlfile
       Filename of the generated HTML file, located in output_loc
    """

    j2_env = Environment(loader=FileSystemLoader(script_dir), trim_blocks=True)
    j2_template = j2_env.get_template(log_template_file)

    sensor_data['weekend_offset'] = 6 - sensor_data['start'].weekday()

    # html output file is same as csv input file, with extension swapped
    htmlfile = os.path.splitext(sensor_data['csvfile'])[0] + ".html"
    sensor_data['htmlfile'] = htmlfile
    j2_template.stream(sensor_data).dump(os.path.join(output_loc, htmlfile))


def create_pdf(html_files: list,
               output_loc: str,
               output_file: str):
    """Create a single PDF from one or more HTML input files.

    Parameters
    ----------
    html_files
       A list of filenames, all located in output_loc. The resulting
       PDF will contain each html_file, one per page
    output_loc
       The temporary directory containing the CSVs and HTML files.
    output_file
       The name of the PDF file to create, which should be located in
       output_loc

    Returns
    -------
    Nothing
    """
    # following values are passed to wkhtmlpdf
    leftrightmargin = 0.75    # in inches
    topbottommargin = 0.5     # in inches
    zoom = 1.0

    # Construct the command line. Options first
    args = ['xvfb-run',
            'wkhtmltopdf',
            '-L', '{}in'.format(leftrightmargin),
            '-R', '{}in'.format(leftrightmargin),
            '-T', '{}in'.format(topbottommargin),
            '-B', '{}in'.format(topbottommargin),
            '--zoom', '{}'.format(zoom)]
    # add all the input files
    args.extend([os.path.join(output_loc, f) for f in html_files])
    # add the output file
    args.append(os.path.join(output_loc, output_file))

    # run it!
    r = subprocess.run(args, timeout=60, check=True)


def email_report(start: date,
                 end: date,
                 output_loc: str,
                 report_file: str,
                 email_to: str):
    """Send the email, attaching the report file

    Parameters
    ----------
    start, end:
       Date objects that define the range of interest, inclusive
    output_loc:
       Path to the temporary directory that contains the report_file
    report_file:
       The file to be emailed.
    email_to:
       The recipient to send the email to. For multiple recipients,
       provide a single string with multiple email addresses separated
       by commans.
    """
    msg = MIMEMultipart()
    msg['From'] = email_from
    msg['To'] = email_to
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = email_subj.format(start, end)

    body = email_body.format(start, end, tempstick_dashboard_url,
                             acct_info.tempstick_username)

    msg.attach(MIMEText(body, 'html'))

    with open(os.path.join(output_loc, report_file), "rb") as f:
        part = MIMEApplication(f.read(), Name=report_file)
        part['Content-Disposition'] = 'attachment; filename="{}"'.\
            format(report_file)
        msg.attach(part)

    smtp = smtplib.SMTP(email_server)
    smtp.sendmail(email_from, email_to.split(","), msg.as_string())
    smtp.close()


def period_to_dates(period: str,
                    relative_to: date = date.today()):
    """Converts a period string into start and end dates.

    Parameters
    ----------
    period:
       Either 'month' (or 'm') or 'week' (or 'w'). The dates will be
       computed to cover the previous month or week, depending on this
       value
    relative_to:
       This will be the anchor date from which the preceeding week or
       month is computed. Normally you want today, but for testing
       this script accepts a parameter.

    Returns
    -------
    start, end:
       A tuple of dates with the appropriate range.

    """
    end = relative_to + timedelta(days=-1)

    if (period == 'month' or period == 'm'):
        if (relative_to.month == 1):            # handle January wrap
            start = date(relative_to.year - 1, 12, relative_to.day)
        else:
            start = relative_to.replace(month=relative_to.month-1)
    elif (period == 'week' or period == 'w'):
        start = relative_to + timedelta(days=-7)
    else:
        raise ValueError("Illegal period {}".format(period))

    return (start, end)


def run_report(period: str,
               email_to: str):
    """Wrapper to do it all: Generate and email the temperature report.

    Parameters
    ----------
    period:
       Either 'month' or 'week'. The report will be computed to cover
       the previous month or week, depending on this value
    email_to:
       The recipient to send the email to. For multiple recipients,
       provide a single string with multiple email addresses separated
       by commas.
    """
    # make temporary directory
    tempdir = tempfile.mkdtemp(prefix='temperature-report-')

    # symlink the logo file into tempdir
    src = os.path.join(script_dir, logo_file)
    dst = os.path.join(tempdir, logo_file)
    os.symlink(src, dst)

    # compute the start and end dates
    (start, end) = period_to_dates(period)

    # get the csv files
    sensor_data_list = get_csvs(tempdir, start, end)

    # process csvs, create html files: one per sensor
    for sensor_data in sensor_data_list:
        import_and_process_csv(sensor_data, tempdir)
        create_html_report(sensor_data, tempdir)

    # collate all the html files into a single pdf
    pdf_file = output_pdf.format(end)
    create_pdf([s['htmlfile'] for s in sensor_data_list],
               tempdir, pdf_file)

    # send email
    email_report(start, end, tempdir, pdf_file, email_to)

    print("Sent file {} to {}".format(pdf_file, email_to, flush=True))

    if acct_info.retain_temp_files:
        print("Temporary directory retained: " + tempdir)
    else:
        shutil.rmtree(tempdir)


if __name__ == '__main__':

    print("====================================")
    print("{} called at {:%c}".format(sys.argv[0], datetime.now()))
    print("Command line: {}".format(" ".join(sys.argv)), flush=True)

    def usage():
        bn = os.path.basename(sys.argv[0])
        usage_msg = ("Usage: {} ( month | week ) <mail_to_address>\n"
                     "\n")

        print(usage_msg.format(bn))

        sys.exit(1)

    if len(sys.argv) != 3:
        print("Incorrect number of arguments!")
        usage()

    period = sys.argv[1].lower()
    email_to = sys.argv[2]

    run_report(period, email_to)
